#!/bin/bash

export HOST='10.11.234.73'
export SYNC_USR='m4p_sync'

copy_ssh_key() {
    echo "Copying ssh key"
    sudo su $SYNC_USR -c 'ssh-copy-id 10.11.234.73'
}

copy_structure() {
    echo "Copying jboss appServer struct"
    sudo su $SYNC_USR -c "ssh -l $SYNC_USR $HOST -o StrictHostKeyChecking=false '(cd /jboss; tar -czvpf - * --exclude=jboss-4.2.3.GA/server/*)' | ( cd /app; sudo tar -xzvf - )"
}

copy_context() {
    echo "Copying jboss context"
    sudo su $SYNC_USR -c "ssh -l $SYNC_USR $HOST -o StrictHostKeyChecking=false '(cd /jboss/appServer/server; tar -cvpf - * --exclude=*tmp/* --exclude=*work/* --exclude=*data/*)' | ( cd /app/appServer/server/; sudo tar -xf - )"
}

copy_jdk() {
    echo "Copying projects jdk"
    sudo su $SYNC_USR -c "ssh -l $SYNC_USR $HOST -o StrictHostKeyChecking=false '(cd /jboss; find . -type l -name jdk|xargs -I {} tar -cvpf - {})' | ( cd /app; sudo tar -xf - )"
}

copy_startup() {
    echo "Copying run.sh configured to supervisor"
    sudo cp /vagrant/files/run.sh.supervisor /app/appServer/bin/run.sh
}

copy_ssh_key
if [[ $? -eq 0 ]]; then
    copy_structure
    copy_context 
    copy_jdk
    copy_startup
fi
