#!/bin/bash

export HOST='10.11.234.74'
export SYNC_USR='vagrant'

copy_structure() {
    ssh -l $SYNC_USR $HOST '(cd /jboss; tar -cvpf - * --exclude=jdk1.7.0_03/* --exclude=jdk1.7.0_72/* --exclude=jboss-4.2.3.GA/server/*)' | ( cd /jboss; sudo tar -xf - )
}

copy_context() {
    ssh -l $SYNC_USR $HOST '(cd /jboss/appServer/server; find . -type l|xargs -I {} tar -cvpf - {} ONGOING)' | ( cd /jboss/appServer/server/; sudo tar -xf - )
}

copy_jdk() {
    ssh -l $SYNC_USR $HOST '(cd /jboss; find . -type l|xargs -I {} tar -cvpf - {})' | ( cd /jboss; sudo tar -xf - )
}

copy_ssh_key() {
    sudo su $SYNC_USR -c 'ssh-copy-id -i /home/m4p_sync/.ssh/id_rsa.pub 10.11.234.74'
}

copy_ssh_key
if [[ $? -eq 0 ]]; then
    copy_structure && copy_context
fi
