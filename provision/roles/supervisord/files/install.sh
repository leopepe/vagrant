#!/bin/bash

pre_req() {
    mkdir -p /etc/supervisor/conf.d
}

install_dep() {
    echo "install setuptools"
    cd setuptools-11.3.1
    python setup.py install
    if [[ $? == 0 ]]; then
        cd ../
    else
        exit 1
    fi

    echo "install elementtree"
    cd elementtree-1.2.6-20050316
    python setup.py install
    if [[ $? == 0 ]]; then
        cd ../
    else
        exit 1
    fi

    echo "install meld3"
    cd meld3-0.6.10
    python setup.py install
    if [[ $? == 0 ]]; then
        cd ../
    else
        exit 1
    fi

    echo "install supervisord"
    cd supervisor-3.1.3
    python setup.py install
    if [[ $? == 0 ]]; then
        cd ../
    else
        exit 1
    fi
}

set_perm() {
    chmod 500 /etc/supervisor
    chmod 500 /etc/supervisor/conf.d
    chmod 0400 /etc/supervisor/supervisord.conf
    chmod 500 /usr/bin/supervisorctl
}

post_install() {
    cp conf/supervisord.sh /etc/init.d/supervisord
    chmod 550 /etc/init.d/supervisord
    chkconfig --add supervisord
    chkconfig supervisord on
    cp conf/supervisord.conf /etc/supervisor/supervisord.conf
    cp conf/supervisord.sysconfig /etc/sysconfig/
}

main() {
    echo "implementing pre-reqs..."
    pre_req
    echo "installing dependencies..."
    install_dep
    echo "post install"
    post_install
    echo "setting permissions"
    set_perm
}

main
